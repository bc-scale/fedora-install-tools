automatized scripts for fedora users (saves a lot of time)

in install.sh replace this line:

    sudo rpm -i dist/<project_name>-<version>-1.noarch.rpm

example:

    sudo rpm -i dist/mygit-0.3-1.noarch.rpm

in uninstall.sh replace this lines:

    sudo rpm -e <project_name>
    sudo rm -r <project_name>.egg-info/

example:

    sudo rpm -e mygit
    sudo rm -r mygit.egg-info/

you can look at setup.py example:
https://gitlab.com/commRat/mygit/-/blob/master/setup.py

tree before executing scripts:

    ├── info.sh
    ├── install.sh
    ├── LICENSE
    ├── mygit
    │   ├── __init__.py
    │   ├── mygit.py
    │   └── VERSION
    ├── mygit.kdev4
    ├── README.md
    ├── setup.py
    └── uninstall.sh

don't forget to "$ chmod +x install.sh" ;)
